<?php
require('login.php');
?>
<!DOCTYPE html>
<html lang="ru" ng-app="app">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Планирование</title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
		<link rel="stylesheet" href="css/main.css" type="text/css">
		<link rel="stylesheet" href="css/animate.min.css" type="text/css">
		<script src="js/jquery-2.1.3.min.js"></script>
		<script src="js/angular.min.js"></script>
		<script src="js/angular-route.min.js"></script>
		<script src="js/angular-locale_ru-ua.js"></script>
		<script src="js/app.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="js/bootstrap-notify.min.js"></script>
		<script>
			var user = {
				userId: <?php echo $userId;?>,
				userName: "<?php echo $userName;?>",
				regionId: <?php echo $regionId;?>,
				regionList: <?php echo json_encode($regionList);?>,
				php_auth_user: "<?php echo $_SERVER['PHP_AUTH_USER'];?>"
			};

			function goToRMR(e){
				document.getElementById("loginFormToPharm").submit();
				return false;
			}
		</script>
	</head>
	<body ng-controller="MainController">
		<header class="container">
			<nav class="navbar navbar-default navbar-inverse navbar-fixed-top" >
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="true">
							<span class="sr-only">Меню</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<span class="navbar-brand" href="#"><img alt="Brand" src="img/logo.png" height="20"></span>
					</div>
					<div class="navbar-collapse collapse" id="navbar-collapse-1" aria-expanded="false">
						<ul class="nav navbar-nav hidden-lg">
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Меню <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="http://yuriapharm.pharmahrm.com/route-medical-representative/">Маршрут МП</a></li>
									<li ng-class="{active:$route.current.scope.name=='planning'}"><a href="#!/">Планирование</a></li>
									<li ng-class="{active:$route.current.scope.name=='planningBrandsCompany'}"><a href="#!/brands-company">План Бренд-ЛПУ</a></li>
									<li ng-class="{active:$route.current.scope.name=='planningCompanyBrands'}"><a href="#!/company-brands">План ЛПУ-Бренд</a></li>
									<li><a href="http://pharmxplorer.com.ua/QvAJAXZfc/opendoc.htm?document=PublishedDocs/basic_yuriapharm.qvw&host=QVS@qlikview&anonymous=true" target="_blank">QlikView <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a></li>
									<!-- <li ng-class="{active:$route.current.scope.name=='statistics'}"><a href="#!/statistics">Статистика</a></li> -->
								</ul>
							</li>
						</ul>
						<ul class="nav navbar-nav visible-lg-block">
							<li>
								<a id="toroute" href="http://yuriapharm.pharmahrm.com/ru/route-medical-representative/" onclick="goToRMR(this);return false;">Маршрут МП</a>
							</li>
							<li class="dropdown active">
								<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Планирование на ацинусы <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li ng-class="{active:$route.current.scope.name=='planning'}"><a href="#!/">Планирование</a></li>
									<li ng-class="{active:$route.current.scope.name=='planningBrandsCompany'}"><a href="#!/brands-company">План Бренд-ЛПУ</a></li>
									<li ng-class="{active:$route.current.scope.name=='planningCompanyBrands'}"><a href="#!/company-brands">План ЛПУ-Бренд</a></li>
								</ul>
							</li>
							<li><a href="http://pharmxplorer.com.ua/QvAJAXZfc/opendoc.htm?document=PublishedDocs/basic_yuriapharm.qvw&host=QVS@qlikview&anonymous=true" target="_blank">QlikView <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a></li>
							<!-- <li ng-class="{active:$route.current.scope.name=='statistics'}"><a href="#!/statistics">Статистика</a></li> -->
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a id="region" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{regionName}} <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li ng-repeat="r in regions" ng-class="{active: r.id==region}">
										<a id="r{{r.id}}" ng-click="setRegion(r.id)">{{r.name}}</a>
									</li>
								</ul>
							</li>
							<li class="dropdown">
								<a id="period" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{periodName}} <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li ng-repeat="p in periods" ng-class="{active: p.id==period}">
										<a id="p{{p.id}}" ng-click="setPeriod(p.id)">{{p.datefrom}} &ndash; {{p.datetill}}</a>
									</li>
								</ul>
							</li>
							<li class="dropdown">
								<a id="user" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Здравствуйте, <?php echo $userFirstName;?> <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li>
										<a href="#" ng-click="logout()">Войти под другим пользователем <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span></a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<div class="container" ng-view></div>

		<form id="loginFormToPharm" action="http://yuriapharm.pharmahrm.com/login_check" method="POST" style="display:none;">
			<input type="hidden" name="_username" value="<?php echo $_SERVER['PHP_AUTH_USER'];?>">
			<input type="hidden" name="_password" value="<?php echo $_SERVER['PHP_AUTH_PW'];?>">
		</form>
		<iframe src="http://yuriapharm.pharmahrm.com/login" style="display:none;"></iframe>
	</body>
</html>