<?php
require_once('connect.php');

function authenticate() {
	header('Content-Type: text/html; charset=utf-8');
    header('WWW-Authenticate: Basic realm="Planning app"');
    header('HTTP/1.0 401 Unauthorized');
    die("Вы должны ввести корректный логин и пароль для получения доступа к ресурсу \n");
}
function reAuthenticate() {
	header('Content-Type: text/html; charset=utf-8');
    echo "<p>Текущий логин: " . htmlspecialchars($_SERVER['PHP_AUTH_USER']) . "<br />";
    echo "<form action='' method='post'>\n";
    echo "<input type='hidden' name='SeenBefore' value='1' />\n";
    echo "<input type='hidden' name='OldAuth' value=\"" . htmlspecialchars($_SERVER['PHP_AUTH_USER']) . "\" />\n";
    echo "<input type='submit' value='Авторизоваться повторно под другим пользователем' />\n";
    echo "</form></p>\n";
}

if (
	!isset($_SERVER['PHP_AUTH_USER'])
	|| (@$_POST['SeenBefore'] == 1 && @$_POST['OldAuth'] == $_SERVER['PHP_AUTH_USER'])
) {
	authenticate();
}
else {
	$res = odbc_exec($connection, "
	SELECT u.id, u.name, ru.region_id, r.Name as region_name FROM info_user u
	LEFT JOIN info_regioninuser ru ON u.id = ru.user_id
	LEFT JOIN info_region r ON ru.region_id = r.id
	WHERE u.po_login = '". $_SERVER['PHP_AUTH_USER'] ."' AND u.po_password ='". $_SERVER['PHP_AUTH_PW'] ."'
	ORDER BY ru.id
	");
	$res_num = odbc_num_rows($res);
	$regionList = array();

	if($res_num){
		$i = 0;
		while($row = odbc_fetch_array($res)){
			if($i==0){
				$userId = $row['id'];
				$userName = iconv("windows-1251", "UTF-8", $row['name']);
				if(count(explode(' ', $userName)) == 3){
					list($userLastName, $userFirstName, $userMiddleName) = explode(' ', $userName);
				}
				else{
					list($userLastName, $userFirstName, $userMiddleName) = array($userName,$userName,$userName);
				}
				$regionId = $row['region_id'];
				if($regionId == NULL){
					echo "Планирование невозможно. Для данного пользователя не установлен регион.<br>\n";
					reAuthenticate();
					exit;
				}

				if(@$_GET['redirect_to'] == 'homepage'){

					echo sprintf('<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="refresh" content="1;url=%1$s" />

        <title>Redirecting to %1$s</title>
    </head>
    <body>
        Redirecting to <a href="%1$s">%1$s</a>.
    </body>
</html>', htmlspecialchars('http://yuriapharm.pharmahrm.com/planning/', ENT_QUOTES, 'UTF-8'));

				}
			}

			// only for yuriapharm:
			// one region for kiev and kiev region
			if($row['region_id']!=28){
				$regionList[] = array('id'=>$row['region_id'], 'name'=>iconv("windows-1251", "UTF-8", $row['region_name']));
			}

			++$i;
		}
	}
	else {
		authenticate();
	}
}