<?php
require('../connect.php');

if(!isset($_POST['companyType']))		die('wrong company type');
if(!isset($_POST['region']))		die('wrong region');

$comanyNameFilter = @$_POST['companyName'] ?
" AND (isnull([info_company].[Name],'') + ' ' + isnull([info_companytype].[Name],'') + ' ' + isnull([info_company].[Building],'') + ' ' +  isnull([info_company].[Street],'') + ' ' + isnull([info_region].[Name],'') + ' ' + isnull([info_city].[Name],'') LIKE '%".
iconv('UTF-8', 'CP1251', $_POST['companyName'])."%')" : '';

switch($_POST['companyType']){
	case 1:
		$companyTypeFilter = "and d.Name like '%стационар%' ";
		break;
	case 2:
		$companyTypeFilter = "and d.Name like '%поликлиника%' ";
		break;
	case 3:
		$companyTypeFilter = "and d.Name not LIKE '%стационар%' and d.Name not like '%поликлиника%'";
		break;
	default:
		$companyTypeFilter = '';
}
if(isset($_POST['existsCompany'])){
	$excludeCompany = 'AND info_company.id NOT IN(';
	$excludeCompany .= implode(',', $_POST['existsCompany']);
	$excludeCompany .= ')';
	$selectedBrands = '';
}
else{
	$excludeCompany = '';
	if(!isset($_POST['selectedBrands']))	die('wrong brands');
	if($_POST['selectedBrands'] == '' || $_POST['selectedBrands'] == 'undefined') exit;
	else{
		$selectedBrands =  "and d.id in (select spec_id from info_preparationspec ps, info_preparation p where ps.prep_id = p.id and p.Brend_id in ({$_POST['selectedBrands']}))";
	}
}

// one region for kiev and kiev region
switch($_POST['region']){
	case 8:
	case 28:
		$region = "AND ( info_region.id = 8 OR info_region.id = 28 ) ";
		break;
	default:
		$region = "AND info_region.id = {$_POST['region']}";
		break;
}

$res = odbc_exec($connection, "
SELECT top 200
   info_company.id AS id
 , info_company.Name AS name
 , isnull(info_city.Name + ', ', '') + ISNULL(lower(StreetType.Name) + ' ','') + ISNULL(info_company.Street, '') + ISNULL(', ' + info_company.Building, '') as addr
FROM  info_company 
LEFT JOIN info_companytype ON info_companytype.id=info_company.CompanyType_id 
LEFT JOIN info_region ON info_region.id=info_company.Region_Id 
LEFT JOIN info_city ON info_city.id=info_company.City_id 
LEFT JOIN info_dictionary AS StreetType ON StreetType.id=info_company.streettype_id 
WHERE isnull(IsArchive,0) = 0
AND isnull(info_companytype.isshop,0)=0
$region
AND exists (select 1 from info_contact c, info_dictionary d where c.company_id = info_company.id and c.Specialization_id = d.id and d.Identifier = 10 $companyTypeFilter $selectedBrands)
$comanyNameFilter
$excludeCompany
ORDER BY info_company.Name
");

if($res){
	$prepare = array();
	while( $row = odbc_fetch_array($res) ) {
		foreach($row as $k=>$v){
			$row[$k] = iconv('CP1251', 'UTF-8', $v);
		}
		$prepare[] = $row;
	}
	// $prepare = $_POST;
	echo json_encode($prepare);
}