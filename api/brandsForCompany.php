<?php
require('../connect.php');
require('../login.php');

if(!isset($_POST['companyId']))		die('wrong company id');
if(!isset($_POST['period']))		die('wrong period');
if(!isset($_POST['region']))		die('wrong region');

// one region for kiev and kiev region
switch($_POST['region']){
	case 8:
	case 28:
		$region = "( c.Region_Id = 8 OR c.Region_Id = 28 ) ";
		break;
	default:
		$region = "c.Region_Id = ". (int)trim($_POST['region'],'"');
		break;
}

$res = odbc_exec($connection, "
select b.id, b.name
, CAST(SUM(csp.cnt) as int) as cnt
, csp.id as salePlanId
from info_companysaleplan csp
inner join info_company c on c.id = csp.company_id
inner join info_preparationbrend b on b.id = csp.brend_id
WHERE csp.brend_id is not null
AND $region
--AND csp.user_id = $userId
AND csp.company_id = ". (int)trim($_POST['companyId'],'"') ."
AND csp.saleperiod_id = ". (int)trim($_POST['period'],'"') ."
group by b.id, b.name, csp.id
order by 1
");

if($res){
	$prepare = array();
	while( $row = odbc_fetch_array($res) ) {
		foreach($row as $k=>$v){
			$row[$k] = iconv('CP1251', 'UTF-8', $v);
		}
		$prepare[] = $row;
	}
	// $prepare = $_POST;
	echo json_encode($prepare);
}