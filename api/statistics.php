<?php
require('../connect.php');
require('../login.php');

if(!@$_POST['period']){
	die('wrong period');
}

$period = (int)trim($_POST['period'],'"');

$res = odbc_exec($connection, "
select
Name
, SUM(cnt_current) as cnt_current
, SUM(cnt1) as cnt1
, SUM(cnt2) as cnt2
, SUM(cnt3) as cnt3
from (
select 
info_company.name
, case when sp.datefrom = sp_current.datefrom then cnt end as cnt_current
, case when DATEDIFF(M, sp.datefrom, sp_current.datefrom) = 1 then cnt end as cnt1
, case when DATEDIFF(M, sp.datefrom, sp_current.datefrom) = 2 then cnt end as cnt2
, case when DATEDIFF(M, sp.datefrom, sp_current.datefrom) = 3 then cnt end as cnt3
from info_companysaleplan
inner join info_company on info_companysaleplan.company_id = info_company.id and info_company.Region_Id = 2
inner join info_saleperiod sp on sp.id = info_companysaleplan.saleperiod_id
inner join (select datefrom from info_saleperiod where id = $period) sp_current on 1 = 1
where saleperiod_id in (select top 4 id from info_saleperiod
      where datefrom <= (select datefrom from info_saleperiod where id = $period)
      order by datefrom desc)

and brend_id = {$_POST['brandId']}
) t
group by Name
");

if($res){
	$prepare = array();
	while( $row = odbc_fetch_array($res) ) {
		foreach($row as $k=>$v){
			$row[$k] = iconv('CP1251', 'UTF-8', $v);
		}
		$prepare[] = $row;
	}
	// $prepare = $_POST;
	header('Content-Type: application/json');
	echo json_encode($prepare);
}