<?php
require('../connect.php');

$res = odbc_exec($connection, "
SELECT id, convert(varchar(10),datefrom, 104) as datefrom, convert(varchar(10),datetill, 104) as datetill from info_saleperiod
where exists (select 1 from info_saleplan2 where SalePeriod_id = info_saleperiod.id)
order by convert(varchar(10),datefrom, 21) desc
");

if($res){
	$prepare = array();
	while( $row = odbc_fetch_array($res) ) {
		$prepare[] = $row;
	}

	echo json_encode($prepare);
}
else{
	echo json_encode(false);
}