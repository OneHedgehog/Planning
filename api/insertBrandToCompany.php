<?php
require_once('../login.php');
require('../connect.php');

if(!@$_POST['brandsId'])	die('wrong brands');
if(!@$_POST['period'])		die('wrong period');
if(!@$_POST['companyId'])	die('wrong company id');

$values = 'VALUES ';
foreach($_POST['brandsId'] as $brandId){
	$values .= "({$_POST['companyId']}, 0, {$_POST['period']}, $userId, $brandId),";
}
$values = trim($values, ',');

$res = odbc_exec($connection, "
INSERT INTO info_companysaleplan(company_id, cnt, saleperiod_id, user_id, brend_id)
$values
");

if($res){
	echo json_encode(true);
}
else{
	echo json_encode(false);
}