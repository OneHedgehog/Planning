<?php
require_once('../login.php');
require('../connect.php');

if(!@$_POST['brandId'])	die('wrong brand id');
if(!@$_POST['period'])	die('wrong period id');
if(!@$_POST['region'])	die('wrong region id');

// one region for kiev and kiev region
switch($_POST['region']){
	case 8:
	case 28:
		$region = "AND ( info_region.id = 8 OR info_region.id = 28 ) ";
		break;
	default:
		$region = "AND info_region.id = {$_POST['region']}";
		break;
}

$res = odbc_exec($connection, "
SELECT top 200
   info_company.id AS id
 , info_company.Name AS name
 , isnull(info_city.Name + ', ', '') + ISNULL(lower(StreetType.Name) + ' ','') + ISNULL(info_company.Street, '') + ISNULL(', ' + info_company.Building, '') as addr
 , csp.cnt
 , csp.id as salePlanId
FROM  info_company 
INNER JOIN info_companysaleplan csp ON csp.company_id = info_company.id
LEFT JOIN info_companytype ON info_companytype.id=info_company.CompanyType_id 
LEFT JOIN info_region ON info_region.id=info_company.Region_Id 
LEFT JOIN info_city ON info_city.id=info_company.City_id 
LEFT JOIN info_dictionary AS StreetType ON StreetType.id=info_company.streettype_id
WHERE info_company.IsArchive = 0
--AND csp.user_id = '$userId'
AND csp.brend_id = '{$_POST['brandId']}'
AND csp.saleperiod_id = '{$_POST['period']}'
$region
ORDER BY info_company.Name
");

if($res){
	$prepare = array();
	while( $row = odbc_fetch_array($res) ) {
		foreach($row as $k=>$v){
			$row[$k] = iconv('CP1251', 'UTF-8', $v);
		}
		$prepare[] = $row;
	}
	// $prepare = $_POST;
	echo json_encode($prepare);
}