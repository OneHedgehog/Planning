<?php
require_once('../login.php');
require('../connect.php');

if(!@$_POST['companiesId'])	die('wrong companies');
if(!@$_POST['period'])		die('wrong period');
if(!@$_POST['brandId'])		die('wrong brand id');

$values = 'VALUES ';
foreach($_POST['companiesId'] as $compId){
	$values .= "($compId, 0, {$_POST['period']}, $userId, {$_POST['brandId']}),";
}
$values = trim($values, ',');

$res = odbc_exec($connection, "
INSERT INTO info_companysaleplan(company_id, cnt, saleperiod_id, user_id, brend_id)
$values
");

if($res){
	echo json_encode(true);
}
else{
	echo json_encode(false);
}