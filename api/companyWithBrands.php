<?php
require_once('../login.php');
require('../connect.php');

if(!isset($_POST['period']))	die('wrong period id');
if(!isset($_POST['region']))	die('wrong region id');

// one region for kiev and kiev region
switch($_POST['region']){
	case 8:
	case 28:
		$region = "AND ( info_company.Region_Id = 8 OR info_company.Region_Id = 28 ) ";
		break;
	default:
		$region = "AND info_company.Region_Id = {$_POST['region']}";
		break;
}

$res = odbc_exec($connection, "
SELECT top 200
   info_company.id AS id
 , info_company.Name AS name
 , isnull(info_city.Name + ', ', '') + ISNULL(lower(StreetType.Name) + ' ','') + ISNULL(info_company.Street, '') + ISNULL(', ' + info_company.Building, '') as addr
 , CAST(SUM(csp.cnt) as int) as cnt
FROM  info_company 
INNER JOIN info_companysaleplan csp ON csp.company_id = info_company.id
LEFT JOIN info_companytype ON info_companytype.id=info_company.CompanyType_id
LEFT JOIN info_city ON info_city.id=info_company.City_id 
LEFT JOIN info_dictionary AS StreetType ON StreetType.id=info_company.streettype_id
WHERE info_company.IsArchive = 0
--AND csp.user_id = '$userId'
AND csp.saleperiod_id = {$_POST['period']}
$region
GROUP BY info_company.id, info_company.Name, info_city.Name, StreetType.Name, info_company.Street, info_company.Building
ORDER BY info_company.Name
");

if($res){
	$prepare = array();
	while( $row = odbc_fetch_array($res) ) {
		foreach($row as $k=>$v){
			$row[$k] = iconv('CP1251', 'UTF-8', $v);
		}
		$prepare[] = $row;
	}

	echo json_encode($prepare);
}