<?php
require('../connect.php');

$res = odbc_exec($connection, "
SELECT TOP 1 id from info_saleperiod where GETDATE() > datetill OR GETDATE() BETWEEN datefrom AND datetill AND exists (
SELECT TOP 1 1
FROM info_saleplan2
WHERE SalePeriod_id = info_saleperiod.id
) ORDER BY datefrom DESC
");

if($res){
	$row = odbc_fetch_array($res);

	echo json_encode((int)$row['id']);
}
else{
	echo json_encode(false);
}