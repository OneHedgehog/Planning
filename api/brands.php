<?php
require('../connect.php');
require('../login.php');

if(isset($_POST['allBrands'])){
	$period = '';
	$region = '';
}
else{
	if(!@$_POST['period']) die('wrong period');
	else {
		$period = " and SalePeriod_id = ". (int)trim($_POST['period'],'"') ."";
	}
	if(!isset($_POST['region']) and !@$_POST['region']) die('wrong region');
	else{
		// one region for kiev and kiev region
		switch($_POST['region']){
			case 8:
			case 28:
				$region = "AND ( Region_Id = 8 OR Region_Id = 28 ) ";
				break;
			default:
				$region = 'AND Region_Id = '.$_POST['region'];
				break;
		}
	}
}

$res = odbc_exec($connection, "
select b.id, b.name
 , CAST(SUM(sp2.[count]) as int) as cnt
 , case when csp.brend_id is null then 0 else 1 end as isPlanned
from info_saleplan2 sp2
inner join info_preparation p on sp2.Preparation_id = p.id
inner join info_preparationbrend b on b.id = p.Brend_id
left join (select distinct brend_id from info_companysaleplan csp, info_company c where c.id = csp.company_id $region $period) csp on csp.brend_id = p.brend_id
where 1=1 $region $period 
group by b.id,b.name,csp.brend_id
order by 2
");

if($res){
	$prepare = array();
	while( $row = odbc_fetch_array($res) ) {
		foreach($row as $k=>$v){
			$row[$k] = iconv('CP1251', 'UTF-8', $v);
		}
		$prepare[] = $row;
	}
	// $prepare = $_POST;
	header('Content-Type: application/json');
	echo json_encode($prepare);
}