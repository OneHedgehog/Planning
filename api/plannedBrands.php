<?php
require('../connect.php');
require('../login.php');

if(!@$_POST['period'])		die('wrong period');
if(!@$_POST['region'])		die('wrong region');

// one region for kiev and kiev region
switch($_POST['region']){
	case 8:
	case 28:
		$region1 = "( c.Region_Id = 8 OR c.Region_Id = 28 ) ";
		$region2 = "( sp.Region_Id = 8 OR sp.Region_Id = 28 ) ";
		break;
	default:
		$region1 = "c.Region_Id = ".(int)trim($_POST['region']);
		$region2 = "sp.Region_Id = ".(int)trim($_POST['region']);
		break;
}

$res = odbc_exec($connection, "
SELECT DISTINCT t2.* FROM
(
SELECT t.brend_id as id, b.name
, SUM(t.cnt) as cnt
, SUM(t.cntOffice) as cntOffice
FROM (
SELECT csp.id, csp.brend_id
, csp.cnt as cnt
, 0 as cntOffice
from info_companysaleplan csp
INNER JOIN info_company c on c.id = csp.company_id
WHERE $region1 AND csp.saleperiod_id = ". (int)trim($_POST['period'],'"') ."
UNION ALL
SELECT 0 as id, p.Brend_id
, 0 as cnt
, CAST(sp.[Count] as int) as cntOffice
from  info_saleplan2 sp
inner join info_preparation p on p.id = sp.Preparation_id
WHERE $region2 AND saleperiod_id = ". (int)trim($_POST['period'],'"') ."
) t
INNER JOIN info_preparationbrend b on b.id = t.brend_id
group by t.brend_id, b.name
) t2
INNER JOIN info_companysaleplan csp2 ON t2.id = csp2.brend_id and csp2.saleperiod_id = ". (int)trim($_POST['period'],'"') ."
INNER JOIN info_company c on c.id = csp2.company_id and $region1
");

if($res){
	$prepare = array();
	while( $row = odbc_fetch_array($res) ) {
		foreach($row as $k=>$v){
			$row[$k] = iconv('CP1251', 'UTF-8', $v);
		}
		$prepare[] = $row;
	}
	// $prepare = $_POST;
	echo json_encode($prepare);
}