angular.module('app', ['ngRoute'])
.factory('brandsIds', function() {
	var brandsIds;
	return {
		get: function(){return brandsIds;},
		set: function(v){brandsIds = v;}
	};
})
.controller('MainController', ['$scope', '$http', '$route', function($scope, $http, $route) {
	// $scope.periods = [{id:30,name:'Период 30'},{id:31,name:'Период 31'},{id:32,name:'Период 32'},{id:33,name:'Период 33'},{id:34,name:'Период 34'}];

	$scope.$route = $route;
	$scope.companyType = 1;
	$scope.company = [];
	$scope.companies = [];
	$scope.brands = [];
	$scope.brandId = 0;

	$http.post(
		'./api/periods.php'
	).success(function(data, status, headers, config) {
		$scope.periods = data;

		$http.post(
			'./api/period.php'
		).success(function(data){
			var periodCurr = $scope.periods.find(function(el){return el.id==data});
			if(typeof periodCurr == 'undefined'){
				$scope.periodName = "Период"
			}
			else{
				$scope.periodName = periodCurr.datefrom + ' – ' + periodCurr.datetill;
			}
			$scope.period = data;
		});
	});

	$scope.regions = user.regionList;
	$scope.region = user.regionList[0].id;
	$scope.regionName = user.regionList[0].name;

	$scope.setPeriod = function(v){
		var periodCurr = $scope.periods.find(function(el){return el.id==v});
		$scope.periodName = periodCurr.datefrom + ' – ' + periodCurr.datetill;
		$scope.period = v;
	}

	$scope.setRegion= function(v){
		var regionCurr = $scope.regions.find(function(el){return el.id==v});
		$scope.regionName = regionCurr.name;
		$scope.region = v;
	}

	$scope.updateSalePlanCount = function(salePlanId, cnt){

		$http.post(
			'./api/updateSalePlanCount.php',
			'salePlanId='+ salePlanId + '&' +
			'companyCnt='+ cnt,
			{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
		).
		success(function(data, status, headers, config) {

			$.notify({
				// options
				message: 'Факт план успешно изменён' 
			},{
				// settings
				type: 'success',
				offset: {x: 20, y: 60},
				delay: 2000,
				animate: {
					enter: 'animated fadeInDown',
					exit: 'animated fadeOut'
				}
			});
		});
	}

	$scope.removeSalePlan = function(salePlanId, param){

		$http.post(
			'./api/removeSalePlan.php',
			'salePlanId='+ salePlanId ,
			{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
		).
		success(function(data, status, headers, config) {

			if(typeof param.companyId != 'undefined')
			$scope.companies.find(function(el){return el.id==param.companyId}).brands = $scope.companies.find(function(el){return el.id==param.companyId}).brands.filter(
				function(el){
					return el.salePlanId!=salePlanId;
				}
			);

			if(typeof param.brandId != 'undefined')
			$scope.brands.find(function(el){return el.id==param.brandId}).companies = $scope.brands.find(function(el){return el.id==param.brandId}).companies.filter(
				function(el){
					return el.salePlanId!=salePlanId;
				}
			);
		});
	}

	$scope.searchCompany = function(name){

		var existsCompanyIds = '';
		var existsCompany = $scope.brands.find(function(el){return el.id==$scope.brandId}).companies;

		for(i in existsCompany){
			if(typeof existsCompany[i].id != 'undefined'){
				existsCompanyIds += 'existsCompany[]='+ existsCompany[i].id + '&';
			}
		}

		$("html").css("cursor", "progress");
		$http.post(
			'./api/company.php',
			'companyType=0&'+
			existsCompanyIds +
			'region='+ $scope.region + '&' +
			'companyName=' + name,
			{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
		).
		success(function(data, status, headers, config) {
			$scope.companies = data;
			$("html").css("cursor", "default");
		}).
		error(function(data, status, headers, config) {
			$("html").css("cursor", "default");
		});
	}

	$scope.logout = function(){

		$http.post(
			'login.php',
			'SeenBefore=1&OldAuth=' + user.php_auth_user,
			{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
		).
		success(function(data, status, headers, config) {
			location.reload();
		});
	}
}])
.controller('BrandsController', ['$scope', '$location', '$http', 'brandsIds', function($scope, $location, $http, brandsIds) {

	$scope.name = "planning";

	$scope.brandsIds = {};

	$scope.$watchGroup(['period','region'], function(value) {
		if(typeof value[0] != 'undefined'){
			$("html").css("cursor", "progress");
			$http.post(
				'./api/brands.php',
				'period='+ value[0] + '&'+
				'region='+ value[1],
				{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
			).
			success(function(data, status, headers, config) {
				$scope.brands = data;
				$("html").css("cursor", "default");
			}).
			error(function(data, status, headers, config) {
				$("html").css("cursor", "default");
			});
		}
	});

	$scope.$watch('brandsIds', function(value) {
		$scope.brandsIdsFiltered = [];
		Object.keys(value).forEach(function (key) {
			if(value[key]){
				$scope.brandsIdsFiltered.push(key);
			}
		});
	}, true);

	$scope.toCompany = function () {
		brandsIds.set($scope.brandsIdsFiltered);

		if(brandsIds.get().length){
			$location.path('/company');
		}
	}
}])
.controller('CompanyController', ['$scope', '$location', '$http', 'brandsIds', function($scope, $location, $http, brandsIds) {

	$scope.name = "planning";

	$scope.searchCompanyString = '';

	$scope.selectedCompany = [];
	$scope.selectedCompanyIds = [];
	if(typeof brandsIds.get() == 'undefined'){
		$location.path('/');
	}

	function findCompanies(){
		$("html").css("cursor", "progress");
		$http.post(
			'./api/company.php',
			'companyType=' + $scope.companyType + '&' +
			'selectedBrands=' + brandsIds.get() + '&' +
			'region='+ $scope.region + '&' +
			'companyName=' + $scope.searchCompanyString,
			{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
		).
		success(function(data, status, headers, config) {
			$scope.$parent.companies = data;
			$("html").css("cursor", "default");
		}).
		error(function(data, status, headers, config) {
			$("html").css("cursor", "default");
		});
	}

	$scope.$watchGroup(['companyType','region'], function(value) {
		findCompanies();
	});

	$scope.searchCompany = function(){
		findCompanies();
	}

	$scope.addCompanyToList = function(id){
		var inSelected = false;
		for(item in $scope.selectedCompany){
			if($scope.selectedCompany[item].id == id){
				inSelected = true; break;
			}
		}
		if(!inSelected){
			var name = $scope.companies.filter(function(v) {return v.id == id})[0].name;
			$scope.selectedCompany.push({id:id, name:name});
			$scope.selectedCompanyIds.push(id);
		}
	}

	$scope.removeCompanyFromList = function(companyId){
		for(item in $scope.selectedCompany){
			if($scope.selectedCompany[item].id == companyId){
				$scope.selectedCompany.splice(item, 1);
				$scope.selectedCompanyIds.splice($scope.selectedCompanyIds.indexOf(companyId), 1);
				break;
			}
		}
	}

	$scope.planning = function(){
		// console.log(brandsIds.get(), $scope.selectedCompanyIds, $scope.period, $scope.region, user.userId);
		$("html").css("cursor", "progress");
		$http.post(
			'./api/planning.php',
			'brandsIds='+ brandsIds.get() +
			'&selectedCompanyIds='+ $scope.selectedCompanyIds +
			'&period='+ $scope.period +
			'&region='+ $scope.region,
			{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
		).
		success(function(data, status, headers, config) {
			alert('Планирование успешно');
			$("html").css("cursor", "default");
		}).
		error(function(data, status, headers, config) {
			alert('planning error');
			$("html").css("cursor", "default");
		});
	}
}])
.controller('BrandsCompanyController', ['$scope', '$location', '$http', function($scope, $location, $http) {

	$scope.name = "planningBrandsCompany";
	$scope.selectedCompany = [];
	$scope.selectedCompanyIds = [];

	$scope.$watchGroup(['period','region'], function(value) {
		if(typeof value[0] != 'undefined'){

			$("html").css("cursor", "progress");
			$http.post(
				'./api/plannedBrands.php',
				'period='+ value[0] + '&'+
				'region='+ value[1],
				{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
			).
			success(function(data, status, headers, config) {
				$scope.$parent.brands = data;
				$("html").css("cursor", "default");
			}).
			error(function(data, status, headers, config) {
				$("html").css("cursor", "default");
			});
		}
	});

	$scope.addCurrentCompany = function(id, name, addr){
		var inSelected = false;
		for(item in $scope.selectedCompanyIds){
			if($scope.selectedCompanyIds[item] == id){
				inSelected = true; break;
			}
		}
		if(!inSelected){
			$scope.selectedCompany.push({id:id, name:name, addr:addr, cnt:0});
			$scope.selectedCompanyIds.push(id);
		}
	}

	$scope.addCompaniesToList = function(id){

		var query_sci = '';
		var existsCompanies = $scope.brands.find(function(el){return el.id==id}).companies;
		for(item in $scope.selectedCompany){
			var currentCompany = $scope.selectedCompany[item];

			if(typeof currentCompany == 'object'){
				var inSelected = false;
				for(exists in existsCompanies){
					if(existsCompanies[exists].id ==currentCompany.id){
						inSelected = true; break;
					}
				}
				if(!inSelected){
					$scope.brands.find(function(el){return el.id==id}).companies.push(currentCompany);
					query_sci += 'companiesId[]='+ currentCompany.id + '&';
				}
			}
		}

		if(query_sci){
			$http.post(
				'./api/insertCompanyToBrand.php',
				query_sci +
				'period='+ $scope.period + '&' +
				'region='+ $scope.region + '&' +
				'brandId='+ id,
				{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
			).
			success(function(data, status, headers, config) {
				//$scope.companies = data;
			});
		}

		$('#add-company').modal('hide');

		var currentAccord = $('#collapse' + id);
		currentAccord.collapse('hide').on('hidden.bs.collapse', function () {
			currentAccord.collapse('show');
		});
		$scope.selectedCompany = [];
		$scope.selectedCompanyIds = [];
	}

	$scope.onChangeCompanyCnt = function(brandId){
		$scope.brands.find(function(el){return el.id==brandId}).cnt = $scope.brands.find(function(el){return el.id==brandId}).companies.sum('cnt');
	}

}])
.controller('CompanyBrandsController', ['$scope', '$location', '$http', function($scope, $location, $http) {
	$scope.name = "planningCompanyBrands";
	$scope.selectedBrands = [];
	$scope.selectedBrandsIds = [];

	$scope.$watchGroup(['period','region'], function(value) {
		if(typeof value != 'undefined' && typeof value[0] != 'undefined'){

			$http.post(
				'./api/companyWithBrands.php',
				'period='+ value[0] + '&'+
				'region='+ value[1],
				{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
			).
			success(function(data, status, headers, config) {
				$scope.$parent.companies = data;
			});
		}
	});

	$scope.addCurrentBrand = function(id, name){
		var inSelected = false;

		for(item in $scope.selectedBrandsIds){
			if($scope.selectedBrandsIds[item] == id){
				inSelected = true; break;
			}
		}
		if(!inSelected){
			$scope.selectedBrands.push({id:id, name:name});
			$scope.selectedBrandsIds.push(id);
		}
	}

	$scope.addBrandsToList = function(id){

		var query_sci = '';
		var existsBrands = $scope.companies.find(function(el){return el.id==id}).brands;
		for(item in $scope.selectedBrands){
			var currentBrand = $scope.selectedBrands[item];

			if(typeof currentBrand == 'object'){
				var inSelected = false;
				for(exists in existsBrands){
					if(existsBrands[exists].id ==currentBrand.id){
						inSelected = true; break;
					}
				}
				if(!inSelected){
					$scope.companies.find(function(el){return el.id==id}).brands.push(currentBrand);
					query_sci += 'brandsId[]='+ currentBrand.id + '&';
				}
			}
		}

		if(query_sci){
			$http.post(
				'./api/insertBrandToCompany.php',
				query_sci +
				'period='+ $scope.period + '&' +
				'companyId='+ id,
				{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
			).
			success(function(data, status, headers, config) {
				//$scope.companies = data;
			});
		}

		$('#add-brand').modal('hide');

		var currentAccord = $('#collapse' + id);
		currentAccord.collapse('hide').on('hidden.bs.collapse', function () {
			currentAccord.collapse('show');
		});
		$scope.selectedBrands = [];
		$scope.selectedBrandsIds = [];
	}
}])
.controller('StatisticsController', ['$scope', '$location', '$http', function($scope, $location, $http) {
	$scope.name = "statistics";

	$scope.$watchGroup(['period','region'], function(value) {
		if(typeof value != 'undefined'){
			periodId = value;

			$("html").css("cursor", "progress");
			$http.post(
				'./api/plannedBrands.php',
				'period='+ value[0] + '&'+
				'region='+ value[1],
				{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
			).
			success(function(data, status, headers, config) {
				$scope.brands = data;
				$("html").css("cursor", "default");
			}).
			error(function(data, status, headers, config) {
				$("html").css("cursor", "default");
			});
		}
	});
}])
.directive('addCompanyModal', ['$http', function($http) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs){

			element.on('show.bs.modal', function (event) {

				scope.$parent.brandId = $(event.relatedTarget).data('brand-id');
				var existsCompanyIds = '';
				var existsCompany = scope.brands.find(function(el){return el.id==scope.brandId}).companies;
				for(i in existsCompany){
					if(typeof existsCompany[i].id != 'undefined')
						existsCompanyIds += 'existsCompany[]='+ existsCompany[i].id + '&';
				}

				$http.post(
					'./api/company.php',
					existsCompanyIds +
					'region='+ scope.region + '&' +
					'companyType='+ 0,
					{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
				).
				success(function(data, status, headers, config) {
					scope.$parent.companies = data;
				});
			});
		}
	};
}])
.directive('addBrandModal', ['$http', function($http) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs){
			element.on('show.bs.modal', function (event) {

				scope.companyId = $(event.relatedTarget).data('company-id');

				$http.post(
					'./api/brands.php',
					'period='+ scope.period + '&'+
					'region='+ scope.region,
					{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
				).
				success(function(data, status, headers, config) {
					scope.brands = data;
				});
			})
		}
	};
}])
.directive('accordionCompany', ['$http', function($http) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs){
			element.on('show.bs.collapse', function (event) {
				var brandId = $(event.target).data('brand-id');

				$http.post(
					'./api/companyForBrands.php',
					'brandId='+ brandId +'&'+
					'period='+ scope.period +'&'+
					'region='+ scope.region,
					{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
				).
				success(function(data, status, headers, config) {
					scope.brands.find(function(el){return el.id==brandId}).companies = data;
					// for(i in data){
						// if(typeof data[i] != 'function')
							// scope.brands.find(function(el){return el.id==brandId}).cntOffice += parseInt(data[i].cnt);
					// }

					scope.brands.find(function(el){return el.id==brandId}).cnt = scope.brands.find(function(el){return el.id==brandId}).companies.sum('cnt');
				});
			})
		}
	};
}])
.directive('accordionBrands', ['$http', function($http) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs){
			element.on('show.bs.collapse', function (event) {
				var companyId = $(event.target).data('company-id');

				$http.post(
					'./api/brandsForCompany.php',
					'companyId='+ companyId + '&' +
					'period=' + scope.period + '&'+
					'region='+ scope.region,
					{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
				).
				success(function(data, status, headers, config) {
					scope.companies.find(function(el){return el.id==companyId}).brands = data;
					// for(i in data){
						// if(typeof data[i] != 'function')
							// scope.brands.find(function(el){return el.id==brandId}).cntOffice += parseInt(data[i].cnt);
					// }

					// scope.brands.find(function(el){return el.id==brandId}).cntOffice = scope.brands.find(function(el){return el.id==brandId}).companies.sum('cnt');
				});
			})
		}
	};
}])
.directive('accordionStatistics', ['$http', function($http) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs){
			element.on('show.bs.collapse', function (event) {
				var brandId = $(event.target).data('brand-id');

				$http.post(
					'./api/statistics.php',
					'brandId='+ brandId + '&'+
					'period='+ scope.period + '&'+
					'region='+ scope.region,
					{ headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
				).
				success(function(data, status, headers, config) {
					scope.brands.find(function(el){return el.id==brandId}).companies = data;
					// for(i in data){
						// if(typeof data[i] != 'function')
							// scope.brands.find(function(el){return el.id==brandId}).cntOffice += parseInt(data[i].cnt);
					// }

					scope.brands.find(function(el){return el.id==brandId}).cntOffice = scope.brands.find(function(el){return el.id==brandId}).companies.sum('cnt');
				});
			})
		}
	};
}])
.config(function($routeProvider, $locationProvider) {
	$routeProvider
	.when('/', {
		templateUrl: 'templates/brands.html',
		controller: 'BrandsController',
	})
	.when('/company', {
		templateUrl: 'templates/company.html',
		controller: 'CompanyController'
	})
	.when('/brands-company', {
		templateUrl: 'templates/brands-company.html',
		controller: 'BrandsCompanyController'
	})
	.when('/company-brands', {
		templateUrl: 'templates/company-brands.html',
		controller: 'CompanyBrandsController'
	})
	.when('/statistics', {
		templateUrl: 'templates/statistics.html',
		controller: 'StatisticsController'
	});

	$locationProvider.html5Mode(false);
	$locationProvider.hashPrefix('!');
});

if (!Array.prototype.find) {
  Array.prototype.find = function(predicate) {
    if (this == null) {
      throw new TypeError('Array.prototype.find called on null or undefined');
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return value;
      }
    }
    return undefined;
  };
}
Array.prototype.sum = function (prop) {
    var total = 0
    for ( var i = 0, _len = this.length; i < _len; i++ ) {
        total += parseInt(this[i][prop]);
    }
    return total
}

function parseQueryString(query,string){
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		if (decodeURIComponent(pair[0]) == string) {
			return decodeURIComponent(pair[1]);
		}
	}
}